'use strict';
import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, hashHistory} from 'react-router';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
const moment = require('moment-timezone');
import reducer from './reducer';
import App from './App';
import {AdminContainer} from './Admin';
import 'antd/dist/antd.css';
import './index.css';

const store = createStore(reducer);
store.dispatch({
  type: 'SET_STATE',
  state: {
    comments: [],
    time: moment().toISOString()
  }
});

ReactDOM.render(
  <Provider store={store}>
    <Router history={hashHistory}>
      <Route path="/" component={App} />
      <Route path="/admin" component={AdminContainer} />
    </Router>
  </Provider>,
  document.getElementById('root')
);
