'use strict';
import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import {connect} from 'react-redux';
import { AdminLoginContainer } from './components/AdminLogin';
import { AdminCommentsContainer } from './components/AdminComments';

export const Admin = React.createClass({
  mixins: [PureRenderMixin], 
  componentWillMount() {
    if (window.localStorage.jwt) {
      this.props.dispatch({
        type: 'SET_STATE', 
        state: {jwt: window.localStorage.jwt}
      });
    }
  },
  render() {
    if (!this.props.jwt) {
      return (
        <AdminLoginContainer />
      );
    }
    return (
      <AdminCommentsContainer />
    );
  }
});

function mapStateToProps(state) {
  return {
    jwt: state.get('jwt'),
    adminComments: state.get('adminComments'),
    logout: state.get('logout')
  };
}

export const AdminContainer = connect(mapStateToProps)(Admin);
