import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import {connect} from 'react-redux';

export const CommentCount = React.createClass({
  mixins: [PureRenderMixin], 
  render() {
    return (<div className="commentcount">
      {this.props.comments.size || 0} comment{this.props.comments && this.props.comments.size === 1 ? '' : 's'}
    </div>);
  }
});

function mapStateToProps(state) {
  return {
    comments: state.get('comments')
  };
}

export const CommentCountContainer = connect(mapStateToProps)(CommentCount);

