'use strict';
const config = require('./config.json');
const uuid = require('node-uuid');
const moment = require('moment');
const akismet = require('akismet-api');
const AWS = require('aws-sdk');
AWS.config.update({region: config.awsRegion});
const simpledb = new AWS.SimpleDB();
const sns = new AWS.SNS();
//const ses = new AWS.SES(); // this is for later, when commenters may want an email on new comments

const akismetClient = akismet.client({
  key  : config.akismetKey,
  blog : config.akismetSite
});
akismetClient.verifyKey((err, valid) => {
  if (err) console.log('Error:', err.message);
  if (!valid) console.log('Invalid akismet key!');
});

const allowedOrigins = config.allowedOrigins.split(',');
const checkOrigin = (event) => {
  return allowedOrigins.indexOf(event.headers.Origin) !== -1;
};
const awsArrayToObject = (arr) => {
  const obj = {};
  for (let i in arr) {
    obj[arr[i]['Name']] = arr[i]['Value'];
  }
  return obj;
};
module.exports.getComments = (event, context, callback) => {
  console.log(event);
  if (!checkOrigin(event)) {
    const response = {
      statusCode: 403,
      body: JSON.stringify({
        message: `You do not seem to be allowed to view these comments`,
      }),
    };
    return callback(null, response);
  }
  // get comments for the referer URL from SimpleDB
  const params =  {
    SelectExpression: `
      select
        displayName,
        commentBody,
        createdAt
      from
        \`${config.SimpleDBDomain}\`
      where
        url = "${event.queryStringParameters.url}" and
        status = "published"
      `,
    ConsistentRead: false
  };
  console.log('select params', params);
  simpledb.select(params, (err, data) => {
    console.log('data', err, data);
    if (err) {
      const response = {
        statusCode: 500,
        headers: {
          "Access-Control-Allow-Origin" : event.headers.Origin // Required for CORS support to work
        },
        body: JSON.stringify({
          message: err,
        }),
      };
      return callback(null, response);
    }
    let comments = [];
    for (let i in data.Items) {
      let item = data.Items[i];
      let attributes = awsArrayToObject(item.Attributes);
      comments.push({
        id: item.Name,
        displayName: attributes.displayName,
        commentBody: attributes.commentBody,
        createdAt: attributes.createdAt
      });
    }
    console.log('comments', comments);
    const response = {
      statusCode: 200,
      headers: {
        "Access-Control-Allow-Origin" : event.headers.Origin // Required for CORS support to work
      },
      body: JSON.stringify({
        message: 'OK',
        comments: comments,
      }),
    };
    return callback(null, response);
  });
};
module.exports.postComment = (event, context, callback) => {
  console.log('received event', event);
  if (!checkOrigin(event)) {
    const response = {
      statusCode: 403,
      body: JSON.stringify({
        message: `You do not seem to be allowed to post comments here`,
      }),
    };
    return callback(null, response);
  }
  if (event.httpMethod === 'OPTIONS') {
    const response = {
      statusCode: 200,
      headers: {
        "Access-Control-Allow-Methods": "GET, POST, OPTIONS",
        "Access-Control-Allow-Origin" : event.headers.Origin 
      },
      body: '',
    };
    return callback(null, response);
  }
  const body = JSON.parse(event.body);
  //console.log('body', body);
  akismetClient.checkSpam({
    user_ip : event.requestContext.identity.sourceIp,              // Required! 
    user_agent : event.headers['User-Agent'],    // Required! 
    referrer : event.headers.Referer,           // Required! 
    permalink : body.url,
    comment_type : 'comment',
    comment_author : body.displayName,
    comment_author_email : body.email,
    //comment_author_url : 'http://johnsblog.com',
    comment_content : body.commentBody,
    is_test: true
  }, (err, spam) => {
    if (err) {
      console.log ('Akismet Error!', err);
    }
    let commentstatus;
    if (spam) {
      commentstatus = 'spam';
    } else {
      commentstatus = config.requireApprovalForNewComments ? 'new' : 'published' ;
    }
    const commentid = uuid.v4();
    const params = {
      DomainName: config.SimpleDBDomain,
      ItemName: commentid,
      Attributes: [
        {
          Name: 'commentBody',
          Value: body.commentBody,
          Replace: false
        },
        {
          Name: 'displayName',
          Value: body.displayName,
          Replace: false
        },
        {
          Name: 'email',
          Value: body.email,
          Replace: false
        },
        {
          Name: 'url',
          Value: body.url,
          Replace: false
        },
        {
          Name: 'status',
          Value: commentstatus,
          Replace: false
        },
        {
          Name: 'createdAt',
          Value: moment().toISOString(),
          Replace: false
        },
      ]
    };
    console.log('putAttributes params', params);
    simpledb.putAttributes(params, (err, data) => {
      if (err) {
        const response = {
          statusCode: 500,
          body: JSON.stringify({
            message: err,
          }),
        };
        return callback(null, response);
      }
      const response = {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Origin" : event.headers.Origin // Required for CORS support to work
        },
        body: JSON.stringify({
          message: 'OK',
          status: commentstatus,
          id: commentid
        }),
      };
      callback(null, response);
      if (config.adminMail && config.notifyAdminOnPostedComment) {
        const mailparams = {
          TopicArn: config.notifyAdminTopic,
          Message: `New comment
Author: ${body.displayName}
URL: ${body.url}
Comment:
${body.commentBody}`,
          Subject: "New comment"
        };
        sns.publish(mailparams, (err, data) => {
          if (err) console.log(err, err.stack); // an error occurred
          else     console.log(data);           // successful response 
        });
      }
    });
  }); 
};

