const getDocHeight = (doc) => {
  doc = doc || document;
  // from http://stackoverflow.com/questions/1145850/get-height-of-entire-document-with-javascript
  //var body = doc.body, html = doc.documentElement;
  var root = doc.getElementById('root');
  var height = Math.max(root.scrollHeight, root.offsetHeight);
  //var height = Math.max( body.scrollHeight, body.offsetHeight, 
  //    html.clientHeight, html.scrollHeight, html.offsetHeight );
  return height;
};

// send docHeight onload
export const sendDocHeightMsg = (e) => {
  var ht = getDocHeight();
  //console.log('ht', ht);
  parent.postMessage( JSON.stringify( {'docHeight': ht} ), '*' );
};


