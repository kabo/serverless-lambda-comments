'use strict';
import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import {connect} from 'react-redux';
const moment = require('moment-timezone');
import { sendDocHeightMsg } from '../utils';
import { Alert, Row, Col, Form, Input, Button } from 'antd';
const FormItem = Form.Item;

export const CommentInput = Form.create()(React.createClass({
  mixins: [PureRenderMixin], 
  componentDidMount() {
    //console.log('didMount');
    sendDocHeightMsg();
  },
  componentDidUpdate() {
    //console.log('didUpdate');
    sendDocHeightMsg();
  },
  postComment(e) {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (err) {
        return;
      }
      //console.log('Received values of form: ', values);
      values.url = this.props.config.url;
      //console.log('Modified values: ', values);
      this.addComment(values);
    });
  },
  addComment(comment) {
    this.props.dispatch({
      type: 'SET_STATE', 
      state: {commentSaveButtonLoading: true}
    });
    fetch(process.env.REACT_APP_postCommentEndpoint, {
        method: 'POST',
        headers: {
          'content-type': 'application/json'
        },
        body: JSON.stringify(comment)
      })
      .then((response) => {
        const j = response.json();
        if (!response.ok) {
          if (j.message) {
            throw j.message;
          }
          throw "Error posting comment. Please try again.";
        }
        return j;
      })
      .then((message) => {
        comment.id = message.id;
        //console.log('comment', comment);
        if (comment.status === 'published') {
          this.props.dispatch({type: 'ADD_COMMENT', state: comment});
        } else {
          this.props.dispatch({type: 'SET_STATE', state: {commentPendingApproval: true}});
        }
        this.props.dispatch({
          type: 'SET_STATE', 
          state: { 
            clearCommentInput: true, 
            commentSaveButtonLoading: false,
            time: moment().toISOString()
          }
        });
      })
      .catch((reason) => {
        this.props.dispatch({
          type: 'SET_STATE', 
          state: {
            comment_input_err: reason.message,
            commentSaveButtonLoading: false
          }
        });
      });
  },
  clearCommentInput() {
    if (this.props.clearCommentInput) {
      this.props.form.resetFields();
      this.props.dispatch({
        type: 'SET_STATE', 
        state: {clearCommentInput: false}
      });
    }
  },
  clearCommentPendingApproval() {
    this.props.dispatch({
      type: 'SET_STATE', 
      state: {commentPendingApproval: false}
    });
  },
  clearCommentErr() {
    this.props.dispatch({
      type: 'SET_STATE', 
      state: {comment_input_err: false}
    });
  },
  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 0 },
      wrapperCol: { span: 24 },
    };
    this.clearCommentInput();
    const renderCommentMessage = () => {
      if (!this.props.commentPendingApproval) {
        return '';
      }
      return (
        <Alert type="success"
          message="Your comment is pending approval"
          closable
          onClose={this.clearCommentPendingApproval} />
      );
    };
    const renderCommentErr = () => {
      if (!this.props.comment_input_err) {
        return '';
      }
      const error = this.props.comment_input_err;
      return (
        <Alert type="error"
          message="Error posting comment"
          description={error}
          closable
          onClose={this.clearCommentErr} />
      );
    };
    return (<div className="CommentInput">
      <Form onSubmit={this.postComment}>
        <FormItem
          {...formItemLayout}
          style={{ marginBottom: 8 }}
          hasFeedback >
          {getFieldDecorator('commentBody', {
            rules: [{ required: true, message: 'Please leave a comment' }],
          })(
            <Input type="textarea" rows={3} placeholder="Comment" />
          )}
        </FormItem>
        <Row gutter={8}>
          <Col span={12} className="gutter-row">
            <FormItem
              {...formItemLayout}
              style={{ marginBottom: 8 }}
              hasFeedback >
              {getFieldDecorator('displayName', {
                rules: [{ required: true, message: 'Please enter your name' }],
              })(
                <Input placeholder="Name" />
              )}
            </FormItem>
          </Col>
          <Col span={12} className="gutter-row">
            <FormItem
              {...formItemLayout}
              style={{ marginBottom: 8 }}
              hasFeedback >
              {getFieldDecorator('email', {
                rules: [
                  { required: true, message: 'Please enter your email' },
                  { type: "email", message: 'Please enter a valid email' }
                ],
              })(
                <Input placeholder="E-mail" />
              )}
            </FormItem>
          </Col>
        </Row>
        <Button 
          type="primary" 
          htmlType="submit" 
          size="large" 
          style={{ marginBottom: 8 }}
          loading={this.props.commentSaveButtonLoading}>
          Comment
        </Button>
        {/* TODO: Checkbox, e-mail me whenever there's new comments */}
      </Form>
      {renderCommentErr()}
      {renderCommentMessage()}
    </div>);
  }
}));

function mapStateToProps(state) {
  return {
    comments: state.get('comments'),
    comment_input_err: state.get('comment_input_err'),
    commentSaveButtonLoading: state.get('commentSaveButtonLoading'),
    clearCommentInput: state.get('clearCommentInput'),
    commentPendingApproval: state.get('commentPendingApproval')
  };
}

export const CommentInputContainer = connect(mapStateToProps)(CommentInput);

