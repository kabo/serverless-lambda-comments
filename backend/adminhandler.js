'use strict';
const config = require('./config.json');
const njwt = require('njwt');
const moment = require('moment');
const AWS = require('aws-sdk');
AWS.config.update({region: config.awsRegion});
const simpledb = new AWS.SimpleDB();
//const akismet = require('akismet-api');
//const akismetClient = akismet.client({
//  key  : config.akismetKey,
//  blog : config.akismetSite
//});
//akismetClient.verifyKey((err, valid) => {
//  if (err) console.log('Error:', err.message);
//  if (!valid) console.log('Invalid akismet key!');
//});

const allowedOrigins = config.allowedOrigins.split(',');
const checkOrigin = (event) => {
  return allowedOrigins.indexOf(event.headers.Origin) !== -1;
};
const awsArrayToObject = (arr) => {
  const obj = {};
  for (let i in arr) {
    obj[arr[i]['Name']] = arr[i]['Value'];
  }
  return obj;
};
module.exports.login = (event, context, callback) => {
  //console.log(event);
  if (!checkOrigin(event)) {
    const response = {
      statusCode: 403,
      body: JSON.stringify({
        message: `You do not seem to be allowed here`,
      }),
    };
    return callback(null, response);
  }
  if (event.httpMethod === 'OPTIONS') {
    const response = {
      statusCode: 200,
      headers: {
        "Access-Control-Allow-Methods": "POST, OPTIONS",
        "Access-Control-Allow-Origin" : allowedOrigins[0] 
      },
      body: '',
    };
    return callback(null, response);
  }
  const info = JSON.parse(event.body);
  if (info.username !== config.username || info.password !== config.password) {
    const response = {
      statusCode: 401,
      headers: {
        "Access-Control-Allow-Origin" : allowedOrigins[0] 
      },
      body: JSON.stringify({
        message: `Invalid login information`,
      }),
    };
    return callback(null, response);
  }
  const claims = {
    iss: 'http://MyApp.com/',
    sub: 'admin',
    exp: moment().add(1, 'day').valueOf()
  };
  const jwt = njwt.create(claims, config.jwtSecret);
  const response = {
    statusCode: 200,
    headers: {
      "Access-Control-Allow-Origin" : allowedOrigins[0] 
    },
    body: JSON.stringify({
      jwt: jwt.compact(),
    }),
  };
  return callback(null, response);
};
module.exports.getComments = (event, context, callback) => {
  //console.log('received event', event);
  if (!checkOrigin(event)) {
    const response = {
      statusCode: 403,
      body: JSON.stringify({
        message: `You do not seem to be allowed here`,
      }),
    };
    return callback(null, response);
  }
  if (event.httpMethod === 'OPTIONS') {
    const response = {
      statusCode: 200,
      headers: {
        "Access-Control-Allow-Methods": "GET, OPTIONS",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Origin" : allowedOrigins[0] 
      },
      body: '',
    };
    return callback(null, response);
  }
  njwt.verify(event.headers.Authorization.substring(7), config.jwtSecret, (err, jwt) => {
    if (err) {
      const response = {
        statusCode: 401,
        headers: {
          "Access-Control-Allow-Methods": "GET, OPTIONS",
          "Access-Control-Allow-Headers": "Authorization",
          "Access-Control-Allow-Origin" : allowedOrigins[0] 
        },
        body: JSON.stringify({
          message: `Invalid login information`,
        }),
      };
      return callback(null, response);
    }
    // get comments for the referer URL from SimpleDB
    const params =  {
      SelectExpression: `
        select
          displayName,
          email,
          commentBody,
          status,
          createdAt,
          url
        from
          \`${config.SimpleDBDomain}\`
        `,
      ConsistentRead: false
    };
    //console.log('select params', params);
    simpledb.select(params, (err, data) => {
      //console.log('data', data);
      if (err) {
        const response = {
          statusCode: 500,
          headers: {
            "Access-Control-Allow-Methods": "GET, OPTIONS",
            "Access-Control-Allow-Headers": "Authorization",
            "Access-Control-Allow-Origin" : allowedOrigins[0] 
          },
          body: JSON.stringify({
            message: err,
          }),
        };
        return callback(null, response);
      }
      let comments = [];
      for (let i in data.Items) {
        let item = data.Items[i];
        let attributes = awsArrayToObject(item.Attributes);
        comments.push({
          id: item.Name,
          displayName: attributes.displayName,
          email: attributes.email,
          commentBody: attributes.commentBody,
          status: attributes.status,
          createdAt: attributes.createdAt,
          url: attributes.url
        });
      }
      //console.log('comments', comments);
      const response = {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Methods": "GET, OPTIONS",
          "Access-Control-Allow-Headers": "Authorization",
          "Access-Control-Allow-Origin" : allowedOrigins[0] 
        },
        body: JSON.stringify({
          message: 'OK',
          comments: comments,
        }),
      };
      return callback(null, response);
    });
  });
};
module.exports.updateComment = (event, context, callback) => {
  //console.log('received event', event);
  if (!checkOrigin(event)) {
    const response = {
      statusCode: 403,
      body: JSON.stringify({
        message: `You do not seem to be allowed here`,
      }),
    };
    return callback(null, response);
  }
  const commentid = event.pathParameters.id;
  const body = JSON.parse(event.body);
  njwt.verify(event.headers.Authorization.substring(7), config.jwtSecret, (err, jwt) => {
    if (err) {
      const response = {
        statusCode: 401,
        headers: {
          "Access-Control-Allow-Methods": "GET, POST, DELETE, OPTIONS",
          "Access-Control-Allow-Headers": "Authorization, Content-Type",
          "Access-Control-Allow-Origin" : allowedOrigins[0] 
        },
        body: JSON.stringify({
          message: `Invalid login information`,
        }),
      };
      return callback(null, response);
    }
    // TODO: submitSpam and submitHam
    const attributes = [];
    ['status'].forEach(name => {
      if (body[name] !== undefined) {
        attributes.push({
          Name: name,
          Value: body[name],
          Replace: true
        });
      }
    });
    const params =  {
      DomainName: config.SimpleDBDomain,
      ItemName: commentid,
      Attributes: attributes
    };
    //console.log('select params', params);
    simpledb.putAttributes(params, (err, data) => {
      //console.log('data', data);
      if (err) {
        const response = {
          statusCode: 500,
          body: JSON.stringify({
            message: err,
          }),
        };
        return callback(null, response);
      }
      //console.log('comments', comments);
      const response = {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Methods": "GET, POST, DELETE, OPTIONS",
          "Access-Control-Allow-Headers": "Authorization, Content-Type",
          "Access-Control-Allow-Origin" : allowedOrigins[0] 
        },
        body: JSON.stringify({
          message: 'OK'
        }),
      };
      callback(null, response);
      return;
      // As we don't currently store ip, user agent or referrer, we can't
      // currently report ham or spam.
//      if (body.status) {
//        // status has been updated, should we report to akismet?
//        if (body.status === 'spam' && body.current.status === 'new') {
//          // admin has marked as spam and it was not caught by akismet, report spam
//          akismetClient.submitSpam({
//            user_ip: '',
//            user_agent: '',
//            referrer: '',
//            permalink: body.current.url,
//            comment_type: 'comment',
//            comment_author: body.current.displayName,
//            comment_author_email: body.current.email,
//            comment_content: body.current.commentBody
//          });
//        }
//        if (body.status === 'published' && body.current.status === 'spam') {
//          // admin is publishing but akismet thought it was spam, report ham
//          akismetClient.submitHam({
//            user_ip: '',
//            user_agent: '',
//            referrer: '',
//            permalink: body.current.url,
//            comment_type: 'comment',
//            comment_author: body.current.displayName,
//            comment_author_email: body.current.email,
//            comment_content: body.current.commentBody
//          });
//        }
//      }
    });
  });
};
module.exports.deleteComment = (event, context, callback) => {
  //console.log('received event', event);
  if (!checkOrigin(event)) {
    const response = {
      statusCode: 403,
      body: JSON.stringify({
        message: `You do not seem to be allowed here`,
      }),
    };
    return callback(null, response);
  }
  const commentid = event.pathParameters.id;
  if (event.httpMethod === 'OPTIONS') {
    const response = {
      statusCode: 200,
      headers: {
        "Access-Control-Allow-Methods": "GET, POST, DELETE, OPTIONS",
        "Access-Control-Allow-Headers": "Authorization, Content-Type",
        "Access-Control-Allow-Origin" : allowedOrigins[0] 
      },
      body: '',
    };
    return callback(null, response);
  }
  njwt.verify(event.headers.Authorization.substring(7), config.jwtSecret, (err, jwt) => {
    if (err) {
      const response = {
        statusCode: 401,
        headers: {
          "Access-Control-Allow-Methods": "GET, POST, DELETE, OPTIONS",
          "Access-Control-Allow-Headers": "Authorization, Content-Type",
          "Access-Control-Allow-Origin" : allowedOrigins[0] 
        },
        body: JSON.stringify({
          message: `Invalid login information`,
        }),
      };
      return callback(null, response);
    }
    const params =  {
      DomainName: config.SimpleDBDomain,
      ItemName: commentid
    };
    //console.log('select params', params);
    simpledb.deleteAttributes(params, (err, data) => {
      //console.log('data', data);
      if (err) {
        const response = {
          statusCode: 500,
          headers: {
            "Access-Control-Allow-Methods": "GET, POST, DELETE, OPTIONS",
            "Access-Control-Allow-Headers": "Authorization, Content-Type",
            "Access-Control-Allow-Origin" : allowedOrigins[0] 
          },
          body: JSON.stringify({
            message: err,
          }),
        };
        return callback(null, response);
      }
      //console.log('comments', comments);
      const response = {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Methods": "GET, POST, DELETE, OPTIONS",
          "Access-Control-Allow-Headers": "Authorization, Content-Type",
          "Access-Control-Allow-Origin" : allowedOrigins[0] 
        },
        body: JSON.stringify({
          message: 'OK'
        }),
      };
      return callback(null, response);
    });
  });
};


