import {Map} from 'immutable';

function setState(state, newState) {
  return state.merge(newState);
}

export default function(state = Map(), action) {
  switch (action.type) {
    case 'SET_STATE':
      return setState(state, action.state);
    case 'ADD_COMMENT':
      return setState(state, state.merge({comments: state.get('comments').push(action.state)}));
  }
  return state;
}
