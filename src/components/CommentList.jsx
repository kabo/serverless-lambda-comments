import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import {connect} from 'react-redux';
import { Row, Col } from 'antd';
import { sendDocHeightMsg } from '../utils';
const moment = require('moment-timezone');

export const CommentList = React.createClass({
  mixins: [PureRenderMixin], 
  componentDidUpdate() {
    //console.log('didUpdate');
    sendDocHeightMsg();
  },
  componentWillMount() {
    fetch(process.env.REACT_APP_getCommentsEndpoint+'?url='+encodeURIComponent(this.props.config.url), {
    }).then(response => {
      //console.log(response);
      const j = response.json();
      //console.log(j);
      if (!response.ok) {
        if (j.message) {
          throw j.message;
        }
        throw "Error getting comments. Please try again.";
      }
      return j;
    }).then(j => {
      this.props.dispatch({
        type: 'SET_STATE', 
        state: {
          comments: j.comments
        }
      });
      //console.log(j);
    }).catch(reason => {
      console.log(reason);
    });
  },
  componentDidMount() {
    this.timerID = setInterval(() => this.props.dispatch({
      type: 'SET_STATE',
      state: { time: moment().toISOString() }
    }), 60000);
  },
  componentWillUnmount() {
    clearInterval(this.timerID);
  },
  render() {
    const userTZ = moment.tz.guess();
    return (<div className="comments">
      {this.props.comments.map(comment => {
        //console.log(comment);
        return (<Row key={comment.get('id')}>
          <Col span={24}>
            <div className="comment">
              <p>
                <span className="comment-author">
                  {comment.get('displayName')}
                </span>
                <span title={comment.get('createdAt')}>
                  {moment(comment.get('createdAt')).tz(userTZ).from(this.props.time)}
                </span>
              </p>
              <p>{comment.get('commentBody')}</p>
            </div>
          </Col>
        </Row>);
      })}
    </div>);
  }
});

function mapStateToProps(state) {
  return {
    comments: state.get('comments'),
    time: state.get('time')
  };
}

export const CommentListContainer = connect(mapStateToProps)(CommentList);

