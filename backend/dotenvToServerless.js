require('dotenv-safe').load();

module.exports = {
  awsAccountNo: function() {
    return process.env.awsAccountNo;
  },
  awsRegion: function() {
    return process.env.awsRegion;
  },
  adminMail: function() {
    return process.env.adminMail;
  },
};
