# Serverless lambda comments

This is going to be a commenting system (think [disqus](https://disqus.com/) for example) that you can run without any servers. Using [Serverless](https://serverless.com/) on [AWS Lambdas](https://aws.amazon.com/lambda/) for backend, [AWS S3](https://aws.amazon.com/s3/) for the [React](https://facebook.github.io/react/) frontend, and [AWS SimpleDB](https://aws.amazon.com/simpledb/) for comment storage.

This is a work in progress, done in my spare time because I enjoy testing out the technology. It'll most likely never be ready for production use, but then again, you never know :)

## How does it work?

After you've deployed the system (see below) you get some HTML to include on your page where you want the comments to appear. This renders an iframe with a static React page hosted in an S3 bucket. This page fetches and posts comments to SimpleDB via the Serverless Lambda backend. 

## How to get it running

### Assumptions:

- You have [AWS CLI](https://aws.amazon.com/cli/) [installed](https://docs.aws.amazon.com/cli/latest/userguide/installing.html) and [configured](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html)
- You have [node](https://nodejs.org/en/) installed

```
$ git clone https://gitlab.com/kabo/serverless-lambda-comments.git
$ cd serverless-lambda-comments
$ cp config.example.json config.json
$ editor config.json
$ npm run deploy
```

Then follow the instructions in the output (copy the indicated parts of the output to your page). Example of what it can look like on a page [here](https://tech.kabo.kiwi/2016/11/23/Serverless-comment-system/).

### config.json

```
awsRegion:                     currently only us-east-1 is supported
adminUsername:                 the username you want to log in to the admin interface with
adminPassword:                 the password you want to log in with to the admin interface
adminEmail:                    e-mail address to send notifications to
requireApprovalForNewComments: true or false
notifyAdminOnPostedComment:    true or false
jwtSecret:                     secret to use when creating and verifying the admin JWT
                               I recommend using some long random string
akismetKey:                    your akismet key
akismetSite:                   your akismet site
mainSiteHosts:                 hosts that the comments are allowed to be used with
                               e.g. if your website is example.com and www.example.com, then
                               write "example.com,www.example.com" here. If someone then tries to
                               load the comments on www.badexample.com they will get an error
                               message and should not be able to read or post comments.
frontendBucketName:            the name of the bucket to store the comment frontend in
```

### Redeploy

If you make any changes (e.g. editing `config.json`) you can redeploy by issuing the following command:

```
$ npm run redeploy
```

## Features

- [x] Login to admin backend (username,password, jwt response, jwt auth for all reqs to lambdas)
- [x] Admin backend for seeing comments
- [x] Admin backend for deleting comments
- [x] Setting to force new comments to be approved by admin
- [x] Admin backend for approving comments
- [x] Akismet for spam filtering
- [x] Admin can mark comments as spam, publish and unpublish comments
- [x] Admin gets an email on new comments
- [x] Somewhat simple to deploy
- [ ] Users can enable autorefresh, to get new comments without having to reload the page
- [ ] Users can log in with Google, Facebook, etc.
- [ ] Users can reply to comments
- [ ] Admin backend reports spam and ham to akismet (requires storing commenter ip, user agent and referrer)
- [ ] Users get an email on new comments if they checked the corresponding checkbox
- [ ] Markdown support?

## FAQ

### Why don't you support ... ?

That sounds like a great idea, I probably haven't thought about that. You're welcome to create a [new issue](https://gitlab.com/kabo/serverless-lambda-comments/issues/new) and describe what you're missing.

### Why not just use Disqus?

Disqus requires you to be logged in to comment. And Disqus, IntenseDebate, etc. can do anything to your readers and their comments and information.

### Other projects

There are other self hosted comment systems out there, e.g. [Isso](https://posativ.org/isso/) (requires a server to run it on) and [lambda-comments](https://github.com/jimpick/lambda-comments) (which I found a bit cumbersome to set up, it's my inspiration to coding this).

### License

Serverless lambda comments is released under the [BipCot License] v1.12 (https://bipcot.org/).

<a href="https://bipcot.org/"><img src="https://www.freedomfeens.com/wp-content/uploads/2016/04/bipicon.jpg" alt="bipicon" /></a>
