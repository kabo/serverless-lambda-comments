'use strict';
if (!window.serverless_lambda_comments_config) {
  window.serverless_lambda_comments_config = {};
}
serverless_lambda_comments_config = Object.assign(serverless_lambda_comments_config, {
  url: window.location.href.substring(window.location.protocol.length)
});
function serverlessLambdaCommentsSendConfig() {
  var ifrm = document.getElementById('serverless-lambda-comments-iframe');
  ifrm.contentWindow.postMessage(JSON.stringify({config: serverless_lambda_comments_config}), '*');
}
function serverlessLambdaCommentsHandleDocHeightMsg(e) {
  if ( e.origin === 'FRONTEND_ORIGIN' ) {
    var data = JSON.parse( e.data );
    if ( data['docHeight'] ) {
      setServerlessLambdaCommentsHeight( data['docHeight'] );
    }
  }
}
function setServerlessLambdaCommentsHeight(height) {
  //console.log(document);
  var ifrm = document.getElementById('serverless-lambda-comments-iframe');
  ifrm.style.visibility = 'hidden';
  ifrm.style.height = height + 4 + "px";
  ifrm.style.visibility = 'visible'; 
}
if ( window.addEventListener ) {
  window.addEventListener('message', serverlessLambdaCommentsHandleDocHeightMsg, false);
} else if ( window.attachEvent ) { // ie8
  window.attachEvent('onmessage', serverlessLambdaCommentsHandleDocHeightMsg);
}
(()=>{
  var comments_placeholder = document.getElementById('serverless-lambda-comments');
  //comments_placeholder.innerHTML = '<iframe scrolling="no" style="background-color: transparent;" width="640" allowtransparency="true" frameborder="0" onload="serverlessLambdaCommentsSendConfig()" id="serverless-lambda-comments-iframe" src="/#/"></iframe>';
  comments_placeholder.innerHTML = '<iframe sandbox="allow-scripts allow-same-origin" scrolling="no" style="background-color: transparent;" width="640" allowtransparency="true" frameborder="0" onload="serverlessLambdaCommentsSendConfig()" id="serverless-lambda-comments-iframe" src="FRONTEND_BUCKET_URL/index.html#/"></iframe>';
})();
