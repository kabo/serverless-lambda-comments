'use strict';

const args = process.argv.slice(2);
if (args.length && args[0] === 'part1') {
  console.log('npm installing...');
  const execSync = require('child_process').execSync;
  execSync('npm install');
  process.exit(0);
}

const fs = require('fs');
const config = require('./config.json');
require('shelljs/global');

let awsAccountNo;
const stackInfo = {
  ServiceEndpoint: '',
  CommentDatabaseOutput: '',
  NewCommentTopicOutput: ''
};
const adminOrigin = `https://${config.frontendBucketName}.s3.amazonaws.com`;
const generateBackendEnv = ({config, stackInfo, awsAccountNo}) => {
  return {
    allowedOrigins: adminOrigin,
    username: config.adminUsername,
    password: config.adminPassword,
    adminMail: config.adminEmail,
    requireApprovalForNewComments: config.requireApprovalForNewComments,
    notifyAdminOnPostedComment: config.notifyAdminOnPostedComment,
    jwtSecret: config.jwtSecret,
    akismetKey: config.akismetKey,
    akismetSite: config.akismetSite,
    awsRegion: config.awsRegion,
    awsAccountNo: awsAccountNo,
    SimpleDBDomain: stackInfo.CommentDatabaseOutput,
    notifyAdminTopic: stackInfo.NewCommentTopicOutput
  };
};

Promise.resolve().then(() => {
  if (args.length && args[0] === 'redeploy') {
    return;
  }
  console.log('npm install backend...');
  cd('backend');
  return new Promise((resolve, reject) => {
    exec('npm install', (code, stdout, stderr) => {
      if (code !== 0) {
        return reject({code, stdout, stderr});
      }
      cd('..');
      return resolve();
    });
  });
}).then(() => {
  console.log('Getting AWS account number...');
  return new Promise((resolve, reject) => {
    exec("aws sts get-caller-identity --output text --query 'Account'", (code, stdout, stderr) => {
      if (code !== 0) {
        return reject({code, stdout, stderr});
      }
      awsAccountNo = stdout.trim();
      return resolve();
    });
  });
}).then(() => {
  if (args.length && args[0] === 'redeploy') {
    return;
  }
  console.log('Generating backend config.json...');
  const backendenv = generateBackendEnv({config, stackInfo, awsAccountNo});
  return new Promise((resolve, reject) => {
    fs.writeFile('backend/config.json', JSON.stringify(backendenv), (err) => {
      if (err) return reject(err);
      return resolve();
    });
  });
}).then(() => {
  if (args.length > 1 && args[0] === 'redeploy' && args[1] === 'frontend') {
    return;
  }
  console.log('Generating backend serverlessvars.json...');
  const vars = {
    "awsRegion": config.awsRegion,
    "awsAccountNo": awsAccountNo,
    "adminMail": config.adminEmail,
    "frontendBucketName": config.frontendBucketName,
    "allowedOrigins": [adminOrigin]
  };
  return new Promise((resolve, reject) => {
    fs.writeFile('backend/serverlessvars.json', JSON.stringify(vars), (err) => {
      if (err) return reject(err);
      return resolve();
    });
  });
}).then(() => {
  if (args.length && args[0] === 'redeploy') {
    return;
  }
  console.log('Deploying backend...');
  cd('backend');
  return new Promise((resolve, reject) => {
    exec("node_modules/serverless/bin/serverless deploy", (code, stdout, stderr) => {
      if (code !== 0) {
        return reject({code, stdout, stderr});
      }
      cd('..');
      return resolve();
    });
  });
}).then(() => {
  console.log('Getting backend stack info...');
  cd('backend');
  return new Promise((resolve, reject) => {
    exec("node_modules/serverless/bin/serverless info --verbose | grep -e ServiceEndpoint -e CommentDatabaseOutput -e NewCommentTopicOutput", (code, stdout, stderr) => {
      if (code !== 0) {
        return reject({code, stdout, stderr});
      }
      stdout.trim()
        .split("\n")
        .map(row => row.split(': '))
        .forEach(row => {
          stackInfo[row[0]] = row[1];
        });
      cd('..');
      return resolve();
    });
  });
}).then(() => {
  if (args.length > 1 && args[0] === 'redeploy' && args[1] === 'frontend') {
    return;
  }
  console.log('Generating backend config.json again...');
  const backendenv = generateBackendEnv({config, stackInfo, awsAccountNo});
  return new Promise((resolve, reject) => {
    fs.writeFile('backend/config.json', JSON.stringify(backendenv), (err) => {
      if (err) return reject(err);
      return resolve();
    });
  });
}).then(() => {
  if (args.length > 1 && args[0] === 'redeploy' && args[1] === 'frontend') {
    return;
  }
  console.log('Deploying backend again...');
  cd('backend');
  return new Promise((resolve, reject) => {
    exec("node_modules/serverless/bin/serverless deploy", (code, stdout, stderr) => {
      if (code !== 0) {
        return reject({code, stdout, stderr});
      }
      cd('..');
      return resolve();
    });
  });
//}).then(() => {
//  console.log('Updating package.json...');
//  sed('-i', 'FRONTEND_BUCKET_NAME', config.frontendBucketName, 'package.json');
}).then(() => {
  console.log('Generating .env...');
  const fenv = `REACT_APP_postCommentEndpoint='${stackInfo.ServiceEndpoint}/comment'
REACT_APP_getCommentsEndpoint='${stackInfo.ServiceEndpoint}/comment'
REACT_APP_adminLoginEndpoint='${stackInfo.ServiceEndpoint}/admin/login'
REACT_APP_adminCommentEndpoint='${stackInfo.ServiceEndpoint}/admin/comment'
REACT_APP_mainSiteHosts='${config.mainSiteHosts}'
`;
  return new Promise((resolve, reject) => {
    fs.writeFile('.env', fenv, (err) => {
      if (err) return reject(err);
      return resolve();
    });
  });
}).then(() => {
  console.log('Building frontend...');
  return new Promise((resolve, reject) => {
    exec("npm run build", (code, stdout, stderr) => {
      if (code !== 0) {
        return reject({code, stdout, stderr});
      }
      return resolve();
    });
  });
}).then(() => {
  console.log('Updating comments.js...');
  sed('-i', 'FRONTEND_BUCKET_URL', adminOrigin, 'build/comments.js');
  sed('-i', 'FRONTEND_ORIGIN', adminOrigin, 'build/comments.js');
}).then(() => {
  console.log('Deploying frontend...');
  return new Promise((resolve, reject) => {
    exec(`aws s3 sync --acl public-read --exclude '*.map' ./build s3://${config.frontendBucketName}`, (code, stdout, stderr) => {
      if (code !== 0) {
        return reject({code, stdout, stderr});
      }
      return resolve();
    });
  });
}).then(() => {
  console.log('Everything has been deployed!');
  console.log(`Please include this javascript in the pages you want to have comments:`);
  console.log(`
<div id="serverless-lambda-comments">Loading comments...</div>
<script async src="${adminOrigin}/comments.js"></script>
`);
  console.log(`You'll find the admin interface here: ${adminOrigin}/index.html#/admin`);
  console.log(' ');
  console.log(`Enjoy!`);
  console.log(' ');
});
// npm install && cd backend && npm install && cd ..
// get aws account number aws sts get-caller-identity --output text --query 'Account'
// read config.json
// generate backend/.env
// generate backend/serverlessvars.yml
// generate .env
// sls deploy
// get stack outputs
//   - frontend bucket
// upload frontend to bucket
// print javascript to put on webpage
// print link to admin page
