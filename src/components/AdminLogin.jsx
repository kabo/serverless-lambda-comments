'use strict';
import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import {connect} from 'react-redux';
import { Icon, Form, Input, Button } from 'antd';
const FormItem = Form.Item;

export const AdminLogin = Form.create()(React.createClass({
  mixins: [PureRenderMixin], 
  handleSubmit(e) {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      //console.log('Received values of form: ', values);
      if (err) {
        return;
      }
      this.props.dispatch({
        type: 'SET_STATE', 
        state: {loggingIn: true}
      });
      fetch(process.env.REACT_APP_adminLoginEndpoint, {
          method: 'POST',
          headers: {
            'content-type': 'application/json'
          },
          body: JSON.stringify(values)
        })
        .then((response) => {
          const j = response.json();
          if (!response.ok) {
            if (j.message) {
              throw j.message;
            }
            throw "Error logging in.";
          }
          return j;
        })
        .then((message) => {
          window.localStorage.jwt = message.jwt;
          this.props.dispatch({
            type: 'SET_STATE', 
            state: {
              loggingIn: false,
              jwt: message.jwt
            }
          });
        });
      // post to backend, place jwt in state and localStorage
    });
  },
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="AdminLogin">
        <Form onSubmit={this.handleSubmit}>
          <FormItem>
            {getFieldDecorator('username', {
              rules: [{ required: true, message: 'Please input your username!' }],
            })(
              <Input addonBefore={<Icon type="user" />} placeholder="Username" />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator('password', {
              rules: [{ required: true, message: 'Please input your Password!' }],
            })(
              <Input addonBefore={<Icon type="lock" />} type="password" placeholder="Password" />
            )}
          </FormItem> 
          <Button type="primary" htmlType="submit" size="large" loading={this.props.loggingIn}>
            Log in
          </Button>
        </Form>
      </div>
    );
  }
}));

function mapStateToProps(state) {
  return {
    jwt: state.get('jwt'),
    loggingIn: state.get('loggingIn')
  };
}

export const AdminLoginContainer = connect(mapStateToProps)(AdminLogin);

