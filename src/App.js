'use strict';
import React from 'react';
import { Alert } from 'antd';
import { CommentCountContainer } from './components/CommentCount';
import { CommentInputContainer } from './components/CommentInput';
import { CommentListContainer }  from './components/CommentList';

const getParentUrl = () => {
  var isInIframe = (parent !== window), parentUrl = null;
  if (isInIframe) {
    parentUrl = document.referrer;
  }
  return parentUrl;
};
const checkParentUrl = (url) => {
  const parser = document.createElement('a');
  parser.href = url;
  return process.env.REACT_APP_mainSiteHosts.indexOf(parser.host) !== -1;
};
export default class App extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      config: {}, 
      parentUrl: getParentUrl()
    };

    if ( window.addEventListener ) {
      window.addEventListener('message', (e) => this.receiveConfig(e), false);
    } else if ( window.attachEvent ) { // ie8
      window.attachEvent('onmessage', (e) => this.receiveConfig(e));
    }
  }
  receiveConfig(e) {
    var data = JSON.parse( e.data );
    if ( data['config'] ) {
      this.setState({config: data['config']});
    }
  }
  render() {
    if (!this.state.parentUrl || 
        !checkParentUrl(this.state.parentUrl) || 
        !this.state.config || 
        !this.state.config.url) {
      return (
        <Alert
          type="error" 
          description="Comments unavailable."
          message="Incorrect config!" />    
      );
    }
    return (<div className="comments">
      <CommentCountContainer config={this.state.config}/>
      <CommentInputContainer config={this.state.config}/>
      <CommentListContainer  config={this.state.config}/>
    </div>);
  }
}

