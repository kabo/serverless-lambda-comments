import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import {connect} from 'react-redux';
import { List } from 'immutable';
import each from 'async/each';
import { Table, Button } from 'antd';
const moment = require('moment-timezone');

export const AdminComments = React.createClass({
  mixins: [PureRenderMixin], 
  logout() {
    delete window.localStorage.jwt;
    this.props.dispatch({
      type: 'SET_STATE', 
      state: { 
        jwt: false
      }
    });
  },
  spamComments() {
    //console.log('rows', this.props.selectedRowKeys);
    this.props.dispatch({
      type: 'SET_STATE', 
      state: { 
        isSpamMarking: true
      }
    });
    each(this.props.selectedRowKeys, (id, callback) => {
      //console.log('id', id);
      this.props.adminComments
        .filter(comment => comment.get('id') === id && comment.get('status') !== 'spam')
        .forEach(comment => this.updateComment({
          id: comment.get('id'), 
          update: {status: 'spam'}, 
          callback
        }));
    }, ( err ) => {
      this.props.dispatch({
        type: 'SET_STATE', 
        state: { 
          isSpamMarking: false
        }
      });
    });
  },
  unpublishComments() {
    //console.log('rows', this.props.selectedRowKeys);
    this.props.dispatch({
      type: 'SET_STATE', 
      state: { 
        isUnpublishing: true
      }
    });
    each(this.props.selectedRowKeys, (id, callback) => {
      //console.log('id', id);
      this.props.adminComments
        .filter(comment => comment.get('id') === id && comment.get('status') !== 'unpublished')
        .forEach(comment => this.updateComment({
          id: comment.get('id'), 
          update: {status: 'unpublished'}, 
          callback
        }));
    }, ( err ) => {
      this.props.dispatch({
        type: 'SET_STATE', 
        state: { 
          isUnpublishing: false
        }
      });
    });
  },
  publishComments() {
    //console.log('rows', this.props.selectedRowKeys);
    this.props.dispatch({
      type: 'SET_STATE', 
      state: { 
        isPublishing: true
      }
    });
    each(this.props.selectedRowKeys, (id, callback) => {
      //console.log('id', id);
      this.props.adminComments
        .filter(comment => comment.get('id') === id && comment.get('status') !== 'published')
        .forEach(comment => this.updateComment({
          id: comment.get('id'), 
          update: {status: 'published'}, 
          callback
        }));
    }, ( err ) => {
      this.props.dispatch({
        type: 'SET_STATE', 
        state: { 
          isPublishing: false
        }
      });
    });
  },
  deleteComments() {
    //console.log('rows', this.props.selectedRowKeys);
    this.props.dispatch({
      type: 'SET_STATE', 
      state: { 
        isDeleting: true
      }
    });
    each(this.props.selectedRowKeys, (id, callback) => {
      //console.log('id', id);
      this.deleteComment({id, callback});
    }, ( err ) => {
      this.props.dispatch({
        type: 'SET_STATE', 
        state: { 
          isDeleting: false,
          selectedRowKeys: []
        }
      });
    });
  },
  updateComment({id, update, callback}) {
    //console.log(arguments);
    //console.log('deleting comment', id);
    fetch(process.env.REACT_APP_adminCommentEndpoint+'/'+id, { 
        method: "POST",
        mode: 'cors',
        headers: {
          'content-type': 'application/json',
          Authorization: `Bearer ${this.props.jwt}`
        },
        body: JSON.stringify(update)
      })
      .then((response) => {
        //console.log('response', response);
        const j = response.json();
        //console.log('j', j);
        if (!response.ok) {
          if (response.status == 401) {
            callback("logout");
            return this.logout();
          }
          if (j.message) {
            throw j.message;
          }
          throw "Error updating comments.";
        }
        return j;
      })
      .then((message) => {
        //console.log('message', message);
        callback();
        this.fetchComments();
      });
  },
  deleteComment({id, callback}) {
    //console.log(arguments);
    //console.log('deleting comment', id);
    fetch(process.env.REACT_APP_adminCommentEndpoint+'/'+id, { 
        method: "DELETE",
        mode: 'cors',
        headers: new Headers({Authorization: `Bearer ${this.props.jwt}`})
      })
      .then((response) => {
        //console.log('response', response);
        const j = response.json();
        //console.log('j', j);
        if (!response.ok) {
          if (response.status == 401) {
            callback("logout");
            return this.logout();
          }
          if (j.message) {
            throw j.message;
          }
          throw "Error getting comments.";
        }
        return j;
      })
      .then((message) => {
        //console.log('message', message);
        callback();
        this.fetchComments();
      });
  },
  getColumns() {
    return [
      {
        title: 'Comment',
        dataIndex: 'commentBody'
      },
      {
        title: 'Author',
        dataIndex: 'displayName',
        render: (text, comment) => `${text}, ${comment.email}`
      },
      {
        title: 'Status',
        dataIndex: 'status'
      },
      {
        title: 'Time',
        dataIndex: 'createdAt'
      },
      {
        title: 'URL',
        dataIndex: 'url',
        render: (text) => (<a href={text} target="_blank">{text}</a>)
      }
    ];
  },
  fetchComments() {
    this.props.dispatch({
      type: 'SET_STATE', 
      state: { 
        isLoading: true
      }
    });
    fetch(process.env.REACT_APP_adminCommentEndpoint, { 
        method: "GET",
        mode: 'cors',
        headers: new Headers({Authorization: `Bearer ${this.props.jwt}`})
        })
    .then((response) => {
      //console.log('response', response);
      const j = response.json();
      //console.log('j', j);
      if (!response.ok) {
        if (response.status == 401) {
          return this.logout();
        }
        if (j.message) {
          throw j.message;
        }
        throw "Error getting comments.";
      }
      return j;
    })
    .then((message) => {
      //console.log('message', message);
      this.props.dispatch({
        type: 'SET_STATE', 
        state: { 
          isLoading: false,
          adminComments: message.comments.map(comment => {
            comment.key = comment.id; 
            return comment;
          })
        }
      });
    });
  },
  componentWillMount() {
    this.props.dispatch({
      type: 'SET_STATE', 
      state: { 
        selectedRowKeys: []
      }
    });
    this.fetchComments();
  },
  getComments() {
    return this.props.adminComments.toJS();
    //return this.props.adminComments.map(comment => {
    //  return comment.toJS();
    //}).toJS();
  },
  onSelectChange(selectedRowKeys) {
    //console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.props.dispatch({
      type: 'SET_STATE', 
      state: { 
        selectedRowKeys: selectedRowKeys
      }
    });
  },
  render() {
    const selectedRowKeys = this.props.selectedRowKeys.toJS();
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;
    return (
      <div className="AdminComments">
        <Button icon="logout" onClick={this.logout} size="large">Log out</Button>
        <Button icon="reload" loading={this.props.isLoading} onClick={this.fetchComments} size="large">Refresh</Button>
        <Button icon="check-circle-o" loading={this.props.isPublishing} disabled={!hasSelected && !this.props.isPublishing} onClick={this.publishComments} size="large">Publish</Button>
        <Button icon="close-circle-o" loading={this.props.isUnpublishing} disabled={!hasSelected} onClick={this.unpublishComments} size="large">Unpublish</Button>
        <Button icon="exclamation-circle-o" loading={this.props.isSpamMarking} disabled={!hasSelected} onClick={this.spamComments} size="large">Mark as spam</Button>
        <Button icon="delete" loading={this.props.isDeleting} disabled={!hasSelected} onClick={this.deleteComments} size="large">Delete</Button>
        <Table loading={this.props.isLoading} rowSelection={rowSelection} dataSource={this.getComments()} columns={this.getColumns()} />
      </div>
    );
  }
});
function mapStateToProps(state) {
  return {
    adminComments: state.get('adminComments') || new List() ,
    jwt: state.get('jwt'),
    isPublishing: state.get('isPublishing'),
    isUnpublishing: state.get('isUnpublishing'),
    isSpamMarking: state.get('isSpamMarking'),
    isDeleting: state.get('isDeleting'),
    isLoading: state.get('isLoading'),
    selectedRowKeys: state.get('selectedRowKeys') || new List()
  };
}

export const AdminCommentsContainer = connect(mapStateToProps)(AdminComments);


